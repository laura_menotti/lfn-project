{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1541f193",
   "metadata": {},
   "source": [
    "# Clustering Uncertain Graphs\n",
    "\n",
    "Here we report the code for our implementation of the SearchKM+ algorithm reported in the paper. For the implementation we use the graph_tool library, that allows to use similar functionalities that are implemented in C++ that are based heavily on the Boost Graph Library. More information about the library are available [Here](https://graph-tool.skewed.de).\n",
    "\n",
    "To measure execution time in Jupyter notebooks: <code>pip install ipython-autotime</code>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "042b7530",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Required libraries\n",
    "import networkx as nx\n",
    "import numpy as np\n",
    "import math\n",
    "import os\n",
    "from pathlib import Path"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "96e42a64",
   "metadata": {},
   "outputs": [],
   "source": [
    "from google.colab import drive\n",
    "drive.mount('/content/drive')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9dbe9387",
   "metadata": {},
   "source": [
    "## SearchKM+ Algorithm\n",
    "\n",
    "Here we report the implementation of some useful functions."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3276e125",
   "metadata": {},
   "source": [
    "### Scores\n",
    "\n",
    "The SearchKM+ algorithm is based on sampling therefore the authors defined some key concepts used in the computation of the best clustering. \n",
    "\n",
    "For any _u_,_v_ $\\in$ _V_ and any random sample _R_ of _G_ we define a function $X_R(u \\sim v)$ such that $X_R(u \\sim v)=1$ if _u_ and _v_ are connected via a path in _R_. For any set $\\mathcal{R}$ of random samples of G and any k-clustering A $\\in$ $\\mathcal{S}_{k}^G$, we define:\n",
    "\n",
    "$ \\widehat{Pr}[\\mathcal{R}, u \\sim v] = \\sum_{R\\in \\mathcal{R}} X_R(u \\sim v)/|\\mathcal{R}| $ \n",
    "\n",
    "that is an unbiased estimator for $Pr[u \\sim v]$. Here we present the function to compute such value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b5352480",
   "metadata": {},
   "outputs": [],
   "source": [
    "def reach_prob(set_samples, u, v):\n",
    "    \n",
    "    result = 0\n",
    "    \n",
    "    for sample in set_samples:\n",
    "        \n",
    "        x_value = 0\n",
    "        # CC: index of the connected component of sample each vertex belongs to\n",
    "        conn_comp = nx.get_node_attributes(sample, \"CC\")\n",
    "        if(conn_comp[u] == conn_comp[v]): \n",
    "            x_value = 1 # v is reachable from u in the sample graph\n",
    "        \n",
    "        result += x_value\n",
    "    \n",
    "    return float(result/len(set_samples))\n",
    "\n",
    "def store_reach_prob(set_samples):\n",
    "    \n",
    "    reach_probabilities = {}\n",
    "    \n",
    "    for n in g.nodes():\n",
    "        for u in g.nodes():\n",
    "            \n",
    "            rp = reach_prob(set_samples, n, u)\n",
    "            if(rp != 0): \n",
    "                try: reach_probabilities[n].update({u : rp})\n",
    "                except KeyError: reach_probabilities.update({n : {u : rp}})\n",
    "\n",
    "    return reach_probabilities"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d889b82",
   "metadata": {},
   "source": [
    "For any _u_,_v_ $\\in$ _V_ and any random sample _R_ of _G_ we define a function $X_R(u\\sim v)$ such that $X_R(u\\sim v)=1$ if _u_ and _v_ are connected via a path in _R_. For any set $\\mathcal{R}$ of random samples of G and any k-clustering A $\\in$ $\\mathcal{S}_{k}^G$, we define:\n",
    "\n",
    "$ \\widehat{KM}[\\mathcal{R}, A] = \\sum_{(u,v)\\in A} \\widehat{Pr}[\\mathcal{R}, u \\sim v]/n $ \n",
    "\n",
    "that is an unbiased estimator for $KM(A)$, the average connection probability of the cluster links in the k-clustering A. Here we present the function to compute such value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9997fe6c",
   "metadata": {},
   "outputs": [],
   "source": [
    "def km_computation(reach_probs, centers):\n",
    "    \n",
    "    sum_prob = 0\n",
    "        \n",
    "    for v in g.nodes():\n",
    "        for c in centers:\n",
    "            try: \n",
    "                sum_prob += reach_probs[v][c]\n",
    "            except KeyError: continue\n",
    "    \n",
    "    return sum_prob/n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b780e9e9",
   "metadata": {},
   "source": [
    "Similarly, for any A $\\in$ $\\mathcal{S}_{k}^G$, any $C \\subseteq V$ and any $v \\in V$, we define:\n",
    "\n",
    "$ \\widehat{f_v}(\\mathcal{R}, C) = \\max \\{\\widehat{Pr}[\\mathcal{R}, u \\sim v] | u \\in C\\} $ \n",
    "\n",
    "$ \\widehat{F}(\\mathcal{R}, C) = \\sum_{v \\in V} \\widehat{f_v}(\\mathcal{R}, C) $ \n",
    "\n",
    "Below we present the function to compute such values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cf9ae611",
   "metadata": {},
   "outputs": [],
   "source": [
    "def max_conn_prob(reach_probs, centers, v):\n",
    "    \n",
    "    p_max = 0\n",
    "\n",
    "    for u in centers:\n",
    "\n",
    "        try: p_curr = reach_probs[u][v]\n",
    "        except KeyError: continue\n",
    "            \n",
    "        if(p_curr > p_max):\n",
    "            p_max = p_curr  \n",
    "    \n",
    "    return p_max\n",
    "    \n",
    "def conn_probs(reach_probs, centers):\n",
    "    \n",
    "    res = 0\n",
    "    \n",
    "    for v in g.nodes():\n",
    "        res += max_conn_prob(reach_probs, centers, v)\n",
    "        \n",
    "    return res"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78b89179",
   "metadata": {},
   "source": [
    "### Function used in LazierGreedy \n",
    "\n",
    "LazierGreedy is the actual subroutine that computes the clusterings. The computation of such function requires some subroutines that are presented below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d8886c80",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Return the collection of connected components of the set of sample graphs\n",
    "def compute_collection_connected_comps(set_samples):\n",
    "\n",
    "    collection_connected_comps = []\n",
    "\n",
    "    for R in set_samples:\n",
    "\n",
    "        components = []\n",
    "        components_tmp = [R.subgraph(c) for c in nx.connected_components(R)]\n",
    "\n",
    "        for cc in components_tmp:\n",
    "            components = cc.nodes()\n",
    "            collection_connected_comps.append(components)\n",
    "\n",
    "    return collection_connected_comps"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c270a1ed",
   "metadata": {},
   "source": [
    "_Something about getUB_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20d94648",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Function that compute new upper bounds of the marginal gains for all v belonging to V     \n",
    "#update ub_v, for each v in V\\C*\n",
    "def getUB(dict_nodes, centers, set_samples, reach_probs, connected_components):\n",
    "\n",
    "    set_size = len(set_samples)\n",
    "\n",
    "    #for each node in the cleaned dict\n",
    "    for key in dict_nodes:\n",
    "      \n",
    "        centers_tmp = list(centers) #centers\n",
    "        centers_tmp.append(key)\n",
    "        h = 0\n",
    "\n",
    "        for cc in connected_components: #all connected comp, computed outside\n",
    "\n",
    "            check =  all(item in cc for item in centers_tmp)\n",
    "            if check is True: #check if cc overlapping al tmp centers\n",
    "                h += len(cc)\n",
    "\n",
    "        #update ub_v\n",
    "        new_ub = min(float(h/set_size) - conn_probs(reach_probs, centers), dict_nodes[key])\n",
    "        dict_nodes[key] = new_ub"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff6e83c2",
   "metadata": {},
   "source": [
    "Something about define\\_clustering "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4795fea7",
   "metadata": {},
   "outputs": [],
   "source": [
    "def define_clustering(g, centers, reach_probs):\n",
    "\n",
    "    nodes = g.nodes()\n",
    "    for n in nodes:\n",
    "\n",
    "        current_c = -1\n",
    "        max_prob = 0.0\n",
    "\n",
    "        #probability links existing for node n\n",
    "        n_dict = reach_probs[n]\n",
    "    \n",
    "        for c in centers:\n",
    "\n",
    "            if c not in n_dict: continue\n",
    "\n",
    "            else: \n",
    "                \n",
    "                tmp_prob = n_dict[c] #connection probability n ~ c\n",
    "    \n",
    "                #check if it is the highest prob. up to now\n",
    "                if tmp_prob > max_prob:\n",
    "                    max_prob = tmp_prob\n",
    "                    current_c = c\n",
    "    \n",
    "        #at the end I have the center that gives the highest prob, \n",
    "        #otherwise -1, if all centers have pr[u ~ c] = 0\n",
    "        G.nodes[n][\"cluster_center\"] = current_c"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "702a94e5",
   "metadata": {},
   "source": [
    "Here we present the algorithm lazierGreedy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0536e5ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "def lazier_greedy(g, k, set_samples, reach_probs, c_comps_coll):\n",
    "    \n",
    "    centers = []\n",
    "    ub_dict = {}\n",
    "\n",
    "    vertices = g.nodes()\n",
    "    \n",
    "    for i in range(1, k+1):\n",
    "\n",
    "        if (i == 1):\n",
    "          \n",
    "            for v in vertices:\n",
    "            # summation of the sizes of all connected components in I(R) that contain v  \n",
    "                l = 0\n",
    "                for cc in c_comps_coll:\n",
    "                    if v in cc:\n",
    "                        l += len(cc)\n",
    "\n",
    "                ub_v = float(l/len(set_samples))\n",
    "                # store result in a dictionary\n",
    "                ub_dict[v] = ub_v\n",
    "        \n",
    "        else:\n",
    "            \n",
    "            #perform update ub_v for each v in ub_dict ( that is V/C*)\n",
    "            getUB(ub_dict, centers, set_samples, reach_probs, c_comps_coll)\n",
    "\n",
    "            #order dict s.t. for v1...vn: UB(v1)>=....>=UB(vn)\n",
    "            sorted_dict = sorted(ub_dict.items(), key=lambda x: x[1], reverse=True)\n",
    "            sorted_nodes = list(dict(sorted_dict).keys())\n",
    "            \n",
    "            for j in range(len(sorted_nodes)-1):\n",
    "                                \n",
    "                tmp = centers.copy()\n",
    "                tmp.append(sorted_nodes[j])\n",
    "                ub_dict[sorted_nodes[j]] = conn_probs(reach_probs, tmp) - conn_probs(reach_probs, centers)\n",
    "                if(ub_dict[sorted_nodes[j]] >= ub_dict[sorted_nodes[j+1]]): break\n",
    "            \n",
    "        # Find v_star in V/C* such that UB(v_star) is maximized\n",
    "        v_star = 0\n",
    "        value = 0.0\n",
    "        for v in ub_dict:\n",
    "            if(ub_dict[v] >= value):\n",
    "                v_star = v\n",
    "                value = ub_dict[v]\n",
    "                \n",
    "        # Update centers accordingly   \n",
    "        centers.append(v_star)\n",
    "        ub_dict.pop(v_star, None)\n",
    "               \n",
    "    return centers"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5b21306",
   "metadata": {},
   "source": [
    "### Functions used in SearchKM+"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6aa74b4f",
   "metadata": {},
   "source": [
    "_Something about sampling_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e62ea55e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Assign to the vertices in the same connected component of sample the same index\n",
    "def connected_components(sample):\n",
    "    \n",
    "    k = 0\n",
    "    attrs = {}\n",
    "    for c in nx.connected_components(sample):\n",
    "        conn_comp = sample.subgraph(c) \n",
    "        for v in conn_comp.nodes(): attrs[v] = {\"CC\": k}\n",
    "        k+=1\n",
    "        \n",
    "    nx.set_node_attributes(sample, attrs)  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fd81f195",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Function that generate size samples of G by removing edge e from g with probability 1-p(e)\n",
    "#GRAPH GENERATION -> MONTECARLO for estimating probs\n",
    "def randomSamples(g, k):\n",
    "    \n",
    "    graph_list_R1 = []\n",
    "    graph_list_R2 = []\n",
    "\n",
    "    for i in range (0, k):\n",
    "\n",
    "        G_tmp1 = nx.Graph(g) #copy, do not modify original graph\n",
    "        G_tmp2 = nx.Graph(g)\n",
    "\n",
    "        for e in g.edges():\n",
    "\n",
    "            p1 = np.random.rand()\n",
    "            p2 = np.random.rand()\n",
    "            \n",
    "            prob_deleting = 1 - float(g[e[0]][e[1]][\"weight\"])\n",
    "\n",
    "            if (p1 < prob_deleting):\n",
    "                G_tmp1.remove_edge(*e)\n",
    "\n",
    "            if (p2 < prob_deleting):\n",
    "                G_tmp2.remove_edge(*e)\n",
    "\n",
    "        graph_list_R1.append(G_tmp1)\n",
    "        graph_list_R2.append(G_tmp2)\n",
    "        \n",
    "        # Compute reachability for later use \n",
    "        connected_components(G_tmp1)\n",
    "        connected_components(G_tmp2)\n",
    "\n",
    "    return graph_list_R1, graph_list_R2;\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c2a12b9f",
   "metadata": {},
   "source": [
    "_Something about lb and ub_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "73ae77d0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Function to compute lb(A*)    \n",
    "def lb_func(centers, reach_probs, alpha, theta):\n",
    "    \n",
    "    km = km_computation(reach_probs, centers)\n",
    "    \n",
    "    partial = ((math.sqrt(km + ((2*alpha)/(9*theta)))) - (math.sqrt(alpha/(2*theta))))**2\n",
    "    \n",
    "    return partial - (alpha/(18*theta))    \n",
    " \n",
    "# Function to compute ub(A°)    \n",
    "def ub_func(centers, reach_probs, alpha, theta):\n",
    "    \n",
    "    km = km_computation(reach_probs, centers)\n",
    "\n",
    "    partial = (math.sqrt((km/(1-(1/math.exp(1)))) + ((8*alpha)/(9*theta))) + math.sqrt(alpha/(2*theta)))**2\n",
    "    \n",
    "    return partial - (alpha/(18*theta))       "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1f65fba",
   "metadata": {},
   "source": [
    "_Something about SearchKM+_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f7fe5836",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Code of the algorithmm searchKM+    \n",
    "def searchKMplus(g, k, eps, delta, t_zero):\n",
    "    \n",
    "    t_max = math.ceil(((2*(7-(7/math.exp(-4*eps)))*(2-math.exp(-1))*n)/(3*k*(eps**2)))*np.log((2*(n**2))/delta))\n",
    "    t = t_zero\n",
    "    \n",
    "    set_samples1, set_samples2 = randomSamples(g, t) \n",
    "    i_max = math.ceil(np.log2(float(t_max/t)))\n",
    "    count = 0\n",
    "    \n",
    "    for i in range(1, i_max+1):\n",
    "        \n",
    "        cc_collection = compute_collection_connected_comps(set_samples1)\n",
    "        reach_probs1 = store_reach_prob(set_samples1)\n",
    "        reach_probs2 = store_reach_prob(set_samples2)\n",
    "        \n",
    "        centers = lazier_greedy(g, k, set_samples1, reach_probs1, cc_collection)\n",
    "        \n",
    "        alpha = np.log((3*i_max)/delta)\n",
    "        theta = len(set_samples1)\n",
    "        \n",
    "        lb = lb_func(centers, reach_probs2, alpha, theta)\n",
    "        ub = ub_func(centers, reach_probs1, alpha, theta)\n",
    "        \n",
    "        if((lb/ub) >= 1-(1/(math.exp(1)-eps)) or i == i_max):\n",
    "            print(\"--- Found the optimal set of centers after\" + str(count) + \"iterations ---\")\n",
    "            print(\"--- Creating the corresponding clustering ---\")\n",
    "            define_clustering(set_samples1, centers)\n",
    "            print(\"--- clustering created ---\")\n",
    "            return centers, reach_probs1\n",
    "        \n",
    "        set_samples1 = randomSample(g, theta*2)\n",
    "        set_samples2 = randomSample(g, theta*2) \n",
    "        count += 1\n",
    "        "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44ae4d12",
   "metadata": {},
   "source": [
    "## Handling Input and Output\n",
    "_some comments_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "67678bef",
   "metadata": {},
   "outputs": [],
   "source": [
    "#LOAD GRAPH, return G and amount of nodes\n",
    "def load_graph(filename):\n",
    "    \n",
    "    G = nx.Graph()\n",
    "    G.clear()\n",
    "    \n",
    "    with open(filename, \"r\") as dataset:\n",
    "\n",
    "        for line in dataset:\n",
    "            tokens = line.split()\n",
    "\n",
    "            node1 = int(tokens[0])\n",
    "            node2 = int(tokens[1])\n",
    "            prob = tokens[2]\n",
    "\n",
    "            G.add_edge(node1, node2, weight = prob)\n",
    "\n",
    "            if node1 not in G:\n",
    "                G.add_node(node1)\n",
    "\n",
    "            if node2 not in G:\n",
    "                G.add_node(node2)\n",
    "\n",
    "    return G, len(G.nodes());"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d59a8d28",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a list of clusters, used for the score computations\n",
    "def store_clustering(g, k):\n",
    "    \n",
    "    unclustered_nodes = [] #store all unclustered nodes\n",
    "    clusters = [] #list of clusters\n",
    "\n",
    "    #create empty list of lists(clusters)\n",
    "    for i in range(k):\n",
    "        clusters.append([])\n",
    "\n",
    "    for n in g.nodes():\n",
    "        #if n unclustered, i.e. pr[n ~ c] = 0, for every center c\n",
    "        if g.nodes[n]['cluster_center'] == -1:\n",
    "            unclustered_nodes.append(n)\n",
    "\n",
    "        else:\n",
    "            #update the list of the related cluster\n",
    "            center = g.nodes[n]['cluster_center']\n",
    "            index = centers.index(center)\n",
    "            clusters[index].append(n)\n",
    "\n",
    "    return unclustered_nodes, clusters"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0516e50",
   "metadata": {},
   "source": [
    "## Executing the K-means clustering\n",
    "In the following cells we execute the code in order to find the optimal clustering for the K-means problem. We store the list of centers, the list of clusters and the reachability probailities in two files in order to compute some clustering score in a different notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "287fe5de",
   "metadata": {},
   "outputs": [],
   "source": [
    "# URLs\n",
    "path = path = str(Path(os.path.abspath(os.getcwd())))\n",
    "inputdir = path + \"/data/datasets/\"\n",
    "inputfile = inputdir + \"collins.txt\"\n",
    "outputdir = path + \"/data/results/\"\n",
    "clusteringfile = outputdir + \"KM_collins.txt\"\n",
    "probabilitiesfile = outputdir + \"RP_collins.txt\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "50a0b3a0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Loading the input graph into memory\n",
    "g, n = load_graph(inputfile)\n",
    "\n",
    "print(\"***Loading input graph completed***\")\n",
    "print(\"nodes: \" + str(n))\n",
    "print(\"edges: \"+ str(len(g.edges)))\n",
    "print(\"--------------------------\")\n",
    "\n",
    "# Parameters\n",
    "k = 69\n",
    "eps = 0.1\n",
    "delta = float(1/n)\n",
    "t_zero = 1000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9986d53f",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time \n",
    "# measures execution time\n",
    "\n",
    "# Executing the clustering\n",
    "centers, reach_probs = searchKMplus(g, k, eps, delta, t_zero)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1c3d50c5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Storing the results in the output files\n",
    "print(\"--- Storing the reachability probabilities in a file ---\")\n",
    "\n",
    "with open(probabilitiesfile, 'w', encoding='utf-8') as f:\n",
    "    \n",
    "    for v in reach_probs.keys():\n",
    "        f.write(str(v) + \" \")\n",
    "        for u in reach_probs[v].keys():\n",
    "            f.write(str(u) + \" \" + str(reach_probs[v][u]) + \" \")\n",
    "        f.write(\"\\n\")\n",
    "        \n",
    "print(\"--- STORING COMPLETED ---\")\n",
    "\n",
    "print(\"--- Storing the centers and the clustering in a file ---\")\n",
    "\n",
    "unclustered_nodes, clusters = store_clustering(g, k)\n",
    "\n",
    "with open(clusteringfile, 'w', encoding='utf-8') as f:\n",
    "    \n",
    "    # storing the list of centers in the first line\n",
    "    for c in centers:\n",
    "        f.write(str(c) + \" \")\n",
    "    f.write('\\n')\n",
    "    # storing the list of unclustered node in a single line\n",
    "    for n in unclustered_nodes:\n",
    "        f.write(str(n) + \" \")\n",
    "    f.write('\\n')\n",
    "    # storing each cluster (i.e, list of nodes belonging to the cluster) in a separate line\n",
    "    for cluster in clusters:\n",
    "        for n in cluster:\n",
    "            f.write(str(n) + \" \")\n",
    "        f.write('\\n')\n",
    "\n",
    "print(\"--- STORING COMPLETED ---\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b8971b1b",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
