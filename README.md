# README #

This repository contains code and data for the _Learning from Networks_ group project.

### Team Members ###

 - Andrea Cassetta andrea.cassetta@studenti.unipd.it
 - Laura Menotti laura.menotti@studenti.unipd.it
 - Riccardo Toffano riccardo.toffano@studenti.unipd.it
 
### The task ###

Starting from two innovative papers we consider the edge uncertainty in an undirected graph and 
study the k-median problem, where the goal is to partition the graph nodes 
into k clusters such that the average connection probability between each 
node and its cluster’s center is maximized.
we decided to implement our version of the SearchKM+ algorithm.

To have a better understanding of our work, please read our final report. 

### The code ###

The code was developed in Python.

Here you can find the two notebooks to run the analysis and than plot the results.



The team proposal file is available at the following [overleaf page](https://it.overleaf.com/project/61767a284590ed2bb51576ca)

